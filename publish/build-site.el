(require 'package)
(setq package-user-dir (expand-file-name "./.packages"))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(package-install 'htmlize)


(require 'ox-publish)

(setq org-html-validation-link nil
      org-html-doctype "html5"
      org-html-html5-fancy t
      org-html-htmlize-output-type 'css
      org-html-head "<link rel=\"stylesheet\" href=\"css/stylesheet.css\" />"
      )

(setq org-publish-project-alist
      '(
        ("dotfiles"
         :recursive nil
         :base-directory "."
         :publishing-directory "./public"
         :publishing-function org-html-publish-to-html
         :htmlized-source t
         :with-author t
         :with-creator t
         :with-toc t
         :section-numbers nil
         :time-stamp-file t
         )
        ("static-assets"
         :recursive t
         :base-directory "./publish/assets"
         :publishing-directory "./public"
         :base-extension any
         :publishing-function org-publish-attachment
         )
        ("all"
         :components ("dotfiles" "static-assets")
         )
        ))

(org-publish-all t)

(message "Build complete")
