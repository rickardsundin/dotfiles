# Ensure that .bashrc is always loaded,
# regardless of if I'm in a login shell or not.

if [ -f ~/.bashrc ]; then . ~/.bashrc; fi
