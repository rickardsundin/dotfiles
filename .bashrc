# MacOS has switched to use zsh as the default shell.
# If you are still on bash, a message gets displayed
# everytime you open a new terminal window:
#   "The default interactive shell is now zsh.
#    To update your account to use zsh, please
#    run `chsh -s /bin/zsh`."
# This code gets rid of that message:

export BASH_SILENCE_DEPRECATION_WARNING=1

# Define some colors for the prompt

##-ANSI-COLOR-CODES-##
Color_Off="\[\033[0m\]"
###-Regular-###
Red="\[\033[0;31m\]"
Green="\[\033[0;32m\]"
Yellow="\[\033[0;33m\]"
Blue="\[\033[0;34m\]"
Purple="\[\033[0;35m\]"
Cyan="\[\033[0;36m\]"
White="\[\033[0;37m\]"
####-Bold-####
BRed="\[\033[1;31m\]"
BPurple="\[\033[1;35m\]"


# Show if there are uncommitted changes in the current
# git repository.

function parse_git_dirty() {
    if [ -n "$(git status --porcelain 2>/dev/null)" ]; then
        echo "*"
    fi
}

# Show name of the current git branch.

function parse_git_branch() {
    git branch --show-current 2>/dev/null
}

# Display the currently active Firebase project.

prompt_firebase() {
    local fb_project=$(grep --no-messages \"$(pwd)\" ~/.config/configstore/firebase-tools.json | cut -d" " -f2)
    if [[ -n $fb_project ]]; then
        echo [$fb_project]
    fi
}

# Archive every command that I have ever run in the terminal
# into a longterm archive.
# See: https://spin.atomicobject.com/2016/05/28/log-bash-history
# NOTE: It requires that the directory =~/.logs= exists.

PROMPT_COMMAND='if [ "$(id -u)" -ne 0 ]; then echo "$(date "+%Y-%m-%d.%H:%M:%S") $(pwd) $(history 1)" >> ~/.logs/bash-history-$(date "+%Y-%m-%d").log; fi'

# Using the functions above, define the prompt.

PS1="$Red\u$Color_Off at $Yellow\h$Color_Off in $Green\w$Color_Off"
PS1+="\$([[ -n \$(git branch 2> /dev/null) ]] && echo \" on \")$Cyan\$(parse_git_branch)\$(parse_git_dirty)$Color_Off $Green\$(prompt_firebase)$Color_Off\n\$ $Color_Off"
PS2="$Yellow→ $Color_Off"


# Some generic useful aliases.
alias ll='ls -al'
alias rm='rm -i'

# List the content of the current directory as an ascii tree.
alias tree='find . -print | sed -e "s;[^/]*/;|____;g;s;____|; |;g"'

# Ensure programs talk to me in english
export LANG=en_US.utf8

###
# OS specific configurations
###
if [[ "$OSTYPE" == "linux-gnu" ]]; then

    # Aliases for MacOS clipboard commands
    alias "pbcopy=xclip"
    alias "pbpaste=xclip -o"

elif [[ "$OSTYPE" == "darwin" ]]; then

    # Specify the Brewfile location.
    export HOMEBREW_BUNDLE_FILE=$HOME/.dotfiles/Brewfile

    # Activate bash completion, if it has been installed
    # with "brew install bash-completion".
    if [ -f $(brew --prefix)/etc/bash_completion ]; then
        . $(brew --prefix)/etc/bash_completion
    fi

    # When sharing the screen, it might be nice to be able to
    # quickly hide a messy desktop.
    alias hidedesktopicons='defaults write com.apple.finder CreateDesktop -bool false && killall Finder'
    alias showdesktopicons='defaults write com.apple.finder CreateDesktop -bool true && killall Finder'
fi

# Generate an UUID as lowercase, put it in the copy buffer and write it
alias uuid="uuidgen | tr '[:upper:]' '[:lower:]' | pbcopy && pbpaste"

###
# Emacs related
###

# open in terminal
alias e='emacsclient --tty'
# open in GUI mode
alias em='emacsclient --create-frame --no-wait'
export EDITOR='emacsclient --tty'
export VISUAL='emacsclient --create-frame'

# Shell-side configurations for running bash in vterm in Emacs.
# Enables directory tracking and prompt tracking.
# https://github.com/akermu/emacs-libvterm#directory-tracking-and-prompt-tracking
vterm_printf() {
    if [ -n "$TMUX" ] && ([ "${TERM%%-*}" = "tmux" ] || [ "${TERM%%-*}" = "screen" ]); then
        # Tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}

vterm_prompt_end(){
    vterm_printf "51;A$(whoami)@$(hostname):$(pwd)"
}
PS1=$PS1'\[$(vterm_prompt_end)\]'

###
# Java related
###

# Quickly change JAVA_HOME to point to the desired JDK.
alias jdk8='export JAVA_HOME=$(/usr/libexec/java_home -v 1.8)'
alias jdk11='export JAVA_HOME=$(/usr/libexec/java_home -v 11)'
alias jdk17='export JAVA_HOME=$(/usr/libexec/java_home -v 17)'
alias jdk21='export JAVA_HOME=$(/usr/libexec/java_home -v 21)'

# Initialize JAVA_HOME to the latest JDK that is installed.
export JAVA_HOME=$(/usr/libexec/java_home)


###
# Ruby related
###

# Enable installing Ruby gems without using sudo
export GEM_HOME="$HOME/.gems"

###
# Node related
###

# Recommended settings when using Node version manager (nvm).
export NVM_DIR="$HOME/.nvm"
# This loads nvm
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
# This loads nvm bash_completion
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

###
# Google Cloud
###

# Adding the Google Cloud CLI tools to PATH, and
# enabling shell command completion for gcloud.

if [ -f '$HOME/Tools/google-cloud-sdk/path.bash.inc' ]; then . '$HOME/Tooles/google-cloud-sdk/path.bash.inc'; fi
if [ -f '$HOME/Tools/google-cloud-sdk/completion.bash.inc' ]; then . '$HOME/Tools/google-cloud-sdk/completion.bash.inc'; fi
