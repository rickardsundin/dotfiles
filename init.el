;; Load main emacs configurations
(org-babel-load-file (concat user-emacs-directory "emacs.org"))

;; Load additional per system configurations, if there are any
(let ((local-config (concat user-emacs-directory "local.org")))
  (if (file-exists-p local-config)
      (org-babel-load-file (concat user-emacs-directory "local.org"))))
